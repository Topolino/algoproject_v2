# Name: Joey Carberry
# Date: December 4, 2015
# Project: Floyd's Triangle Project
num = 1
n = int(input('Number of Rows: '))
nSum = (n^2 + 1)/2
y = 1
numString = ''
count = 1
add = 1
total = 0
rowSum = 0
for a in range(0,n):
    total += add
    add += 1
while y <= total:
    for b in range(0, count):
        rowSum += num
        numString += str(num)
        numString += ","
        num += 1
        y += 1
    print(numString, '     Total:', rowSum)
    rowSum = 0
    numString = ''
    count += 1



